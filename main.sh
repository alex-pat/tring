#!/bin/bash

myncat() {
    while ! ncat "$1" "$2" 2>/dev/null
    do
        sleep 0.1
    done
}

if [ -z "$5" ]; then
   echo "Usage: $0 LISTEN_IP LISTEN_PORT WRITE_IP WRITE_PORT NAME [-m]"
   exit 1
fi

ncat -l "$1" "$2" | python3 ./main.py "$5" "$6" | myncat "$3" "$4"
